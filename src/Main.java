import java.util.*;

public class Main {

    public static void main(String[] args) {
        /*this is better performance*/
        BalancedGeneratorDP generator = new BalancedGeneratorDP();
        System.out.println(generator.permutations(10));

        /*this is less efficient*/
        // todo Please uncomment this to try less efficient solution
        /*BalancedGeneratorLogicTable generatorLT = new BalancedGeneratorLogicTable();
        System.out.println(generatorLT.permutations(10));*/
    }
}

/**
 * Dynamic programming principle applied where the solution is constructed based on the decomposition principle
 * O(n^2) since there are 2 loops
 * in order to improve the result I'm building front to back strategy and not back to front
 * since front to back converges more quickly to the solution and less steps to calculate because it just builds the
 * necessary sub states.
 * Permutations follow the following counting principle ie: if n=4  => _ _ _ _ === 2*2*2*2= 2^n. I've taught discrete mathematics in the past.
 * So for n= 30  expected permutations are 1'073.741.824 this works in about 700 milliseconds in my intel 3.1 Core i7
 * So for n= 34  expected permutations are 17'179.869.180 this works in about 7000 milliseconds
 * So for n= 40  expected permutations are 1.099'511.628.000 this last too much but not cause memory leak.
 * since it trends to infinite it does not make sense try to be computed
 * In order to soften memory issues I'm deleting sub problem entries that will not be used in the next steps
 **/

class BalancedGeneratorDP {
    private Map<Integer, Set<String>> cache = new HashMap<>();
    private static final String BASE = "()";
    private static final String OPEN = "(";
    private static final String CLOSE = ")";

    BalancedGeneratorDP() {
        Set<String> entry = new HashSet<>();
        entry.add(BASE);
        cache.put(2, entry);
    }

    Set<String> permutations(int n) {
        Date init = new Date();
        if (n > 2) {
            for (int i = 4; i <= n; i = i + 2) {
                // iterate known values
                Set<String> subScenarioResult = new HashSet<>();
                for (String items : cache.get(i - 2)) {
                    String scenarioA = BASE + items;// ()...
                    String scenarioB = items + BASE;// ...()
                    String scenarioC = OPEN + items + CLOSE; // (...)
                    subScenarioResult.add(scenarioA);
                    subScenarioResult.add(scenarioB);
                    subScenarioResult.add(scenarioC);
                }
                cache.put(i, subScenarioResult);
                if (i > 2) {
                    cache.remove(i - 2);
                }
            }
        }
        Date end = new Date();
        long elapsed = end.getTime() - init.getTime();
        System.out.println("Elapsed Time in milliseconds: " + elapsed);
        return cache.get(n);
    }
}

/**
 * This solution will take the logic tables where all permutations are build based on the swapping principle
 * ie. for a 4 column size table it is necessary 2^4 rows for first column half are false and half are true
 * and for the second column you look the first column half and inside it fill half false and half true and so on.
 * This is also O(n^2) algorithm but not using cache as the DP solution so it takes much more time by far.
 * In both solutions balancing is been computed in the way of generating the script in order to avoid iterate again.
 * In case iterating again to check if it is balanced or not the solution would be O(n^3)
 **/

class BalancedGeneratorLogicTable {
    private static final String OPEN = "(";
    private static final String CLOSE = ")";

    Set<String> permutations(int n) {
        Set<String> result = new HashSet<>();
        Date init = new Date();
        Long openCount = 0L;
        Long balance = 0L;
        Double rows = Math.pow(2, n - 1); // we need just half starting in (
        for (int row = 0; row < rows; row++) {
            StringBuilder rowPermutation = new StringBuilder();
            boolean isBalanced = true;
            for (int column = 0; column < n; column++) {
                Double swappingModule = Math.pow(2, n - column - 1);// determine module to swap
                int swappingResult = (int) (row / swappingModule);
                if (swappingResult % 2 == 0) {// odd= close; even=open
                    if (openCount < n / 2) {
                        rowPermutation.append(OPEN);
                        openCount++;
                        balance++;
                    } else {
                        isBalanced = false;
                        break;
                    }
                } else {
                    if (balance > 0) {
                        rowPermutation.append(CLOSE);
                        balance--;
                    } else {
                        isBalanced = false;
                        break;
                    }
                }
            }
            if (isBalanced) {
                // todo enable this if you want printing the results on the fly
                //System.out.println(rowPermutation.toString());
                result.add(rowPermutation.toString());
            }
            openCount = 0L;
            balance = 0L;
        }

        Date end = new Date();
        long elapsed = end.getTime() - init.getTime();
        System.out.println("Elapsed Time in milliseconds: " + elapsed);
        return result;
    }
}